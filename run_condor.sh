#!/bin/bash
# How to run ./run_condor.sh data_22 ttbar_a vv_a z_jet_a data_23 ttbar_d vv_d z_jet_d ttdb_a ttdb_d llgamma_a llgamma_d 2>&1 &
# Use --sum-only to only sum the histograms without submitting the jobs

directory=$(dirname "$(realpath "$0")")
directory=${directory}"/"
sum_only=false
for i in "$@"; do
  if [ "$i" == "--sum-only" ]; then
    sum_only=true
  else
    newparams+=("$i")
  fi
done
set -- "${newparams[@]}"

declare -a jobs
jobs=("data_22" "ttbar_a" "vv_a" "z_jet_a" "data_23" "ttbar_c" "vv_c" "z_jet_c" "ttbar_d" "vv_d" "z_jet_d" "ttdb_a" "ttdb_c" "ttdb_d" "llgamma_a" "llgamma_c" "llgamma_d")
# Array with all the possible logs
declare -A logs
logs=(
  ["data_22"]="${directory}histos/data/condor_data_22.log"
  ["data_23"]="histos/data/condor_data_23.log"
  ["ttbar_a"]="histos/ttbar/condor_ttbar_a.log"
  ["ttbar_c"]="histos/ttbar/condor_ttbar_c.log"
  ["ttbar_d"]="histos/ttbar/condor_ttbar_d.log"
  ["vv_a"]="histos/vv/condor_vv_a.log"
  ["vv_c"]="histos/vv/condor_vv_c.log"
  ["vv_d"]="histos/vv/condor_vv_d.log"
  ["z_jet_a"]="histos/z_jet/condor_z_jet_a.log"
  ["z_jet_c"]="histos/z_jet/condor_z_jet_c.log"
  ["z_jet_d"]="histos/z_jet/condor_z_jet_d.log"
  ["ttdb_a"]="histos/ttdb/condor_ttdb_a.log"
  ["ttdb_c"]="histos/ttdb/condor_ttdb_a.log"
  ["ttdb_d"]="histos/ttdb/condor_ttdb_a.log"
  ["llgamma_a"]="histos/llgamma/condor_llgamma_a.log"
  ["llgamma_c"]="histos/llgamma/condor_llgamma_a.log"
  ["llgamma_d"]="histos/llgamma/condor_llgamma_a.log"
)

# Check if arguments were provided
if [ "$#" -eq 0 ]; then
  echo "No job specified. Please provide one or more of the following jobs as arguments:"
  echo "${jobs[@]}"
  exit 1
fi

if [ "$sum_only" = false ]; then
  # Submit the specified jobs
  mkdir -p histos
  for job in "$@"; do
    if [ -n "${logs[$job]}" ]; then
      mkdir -p histos/${job%_*}
      mkdir -p histos/${job%_*}/output
      mkdir -p histos/${job%_*}/error
      sample=${job%_*}
      year=${job##*_}
      condor_submit -a SAMPLE=$sample -a YEAR=$year -a DIRECTORY=$directory condor/job.submit
      echo "Job $job submitted"
    else
      echo "Job $job not recognized"
    fi
  done

  echo " "
  echo "All requested jobs have been submitted"
  echo " "

  # Check the status of the specified jobs
  for job in "$@"; do
    if [ -n "${logs[$job]}" ]; then
      condor_wait "${logs[$job]}"
      echo "Job $job finished"
    else
      echo "Log for job $job not found"
    fi
  done

  echo " "
  echo "All requested jobs have finished"
  echo " "
fi

# Run sum_histo.py for the specified jobs
for job in "$@"; do
  if [[ $job == data_* ]]; then
    year=${job#data_}
    python3 sum_histo.py --dataset data --year $year
  else
    dataset=${job%_*}
    campaign=${job##*_}
    python3 sum_histo.py --dataset $dataset --campaign $campaign --year 23
  fi
  echo "Histograms summed for job $job"
done

echo " "
echo "All requested histograms have been summed"
echo " "