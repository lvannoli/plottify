import ROOT as r
import sys
import os
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import argparse
from math import pi, ceil
import multiprocessing
from multiprocessing import Pool, Manager
from tqdm import tqdm
import json

with open('configs.json') as f:
    configs = json.load(f)

# Function to fill histograms
def fill_histograms(histogram, dataframe, column_name):
    data = dataframe[column_name].to_numpy().astype(float)
    weight = dataframe["weight"].to_numpy().astype(float)
    #histogram.FillN(len(dataframe), data, np.ones(len(dataframe)), weight)
    histogram.FillN(len(dataframe), data, weight)

# Function to create histograms
def create_histograms(prefix):
    histograms = {}
    for key, value in configs['histogram_props'].items():
        hist = r.TH1F(f'h_{prefix}_{key}', f'{prefix.capitalize()} {key}', value[0], value[1], value[2])
        x_axis_name, y_axis_name = value[3], value[4]
        hist.GetXaxis().SetTitle(x_axis_name)
        hist.GetYaxis().SetTitle(y_axis_name)
        histograms[f'{prefix}_{key}'] = hist
    return histograms

def process_tree_entry(tree):  # 'tree' instead of 'entry'
    lepton_data = {}
    for col in configs['histogram_props'].keys():
        try:
            lepton_data[col] = [getattr(tree, col)[i] for i in range(len(getattr(tree, col)))]
        except:
            lepton_data[col] = getattr(tree, col)
    return lepton_data

# Function to process a tree entry
def process_file(file_info, idx, print_queue):
    file_name = file_info
    data_list = [] # list to store the data
    file_path = os.path.join(in_folder, file_name)
    # Check if the file name contains the dataset and the year
    if any(item in file_name for item in configs['dataset'][args.dataset]) and args.year[-2:] in file_name:
        # Check if the file number is in the list of file numbers
        if args.file_number:
            mc_number = int(file_name.split('.')[1])
            # If the file number is not in the list of file numbers, skip the file
            if mc_number not in args.file_number:
                return []
        # Open the ROOT file
        f = r.TFile.Open(file_path)
        # Get the tree from the file
        tree = f.Get(configs['trees'][0])

        # Get the number of entries in the tree
        num_entries = tree.GetEntries()
        if args.batch_num!=0:
            print_queue.put(f'Batch number: {args.batch_num}')
            try:
                start = args.ev_batch * (args.batch_num - 1)
                end = start + args.ev_batch
                print_queue.put(f'File: {file_name}\nNumber of entries: {args.ev_batch}')
            except:
                try:
                    start = ceil(num_entries/args.max_batches) * (args.batch_num - 1)
                    end = start + ceil(num_entries/args.max_batches)
                    print_queue.put(f'File: {file_name}\nNumber of entries: {ceil(num_entries/args.max_batches)}')
                except:
                    start = 0
                    end = num_entries
                    print_queue.put(f'File: {file_name}\nNumber of entries: {num_entries}')
        else:
            start = 0
            end = num_entries
            print_queue.put(f'File: {file_name}\nNumber of entries: {num_entries}')

        with tqdm(total=end-start, desc=f'Processing file {file_name}', position=idx, disable=args.condor) as pbar:
            # Loop ove the entries in the tree
            for i in range(start, end):
                # Process the entry and append the data to the list
                tree.GetEntry(i)
                data_list.append(process_tree_entry(tree))  # pass 'tree' instead of 'entry'
                # Update the progress bar
                pbar.update(1)
                # If we are running in test mode and the loop reached the maximum numnber of events to be processes, break the loop
                if len(data_list) == args.test:
                    break
        # Close the ROOT file
        f.Close()
    return data_list

def generate_output_file_path(args, output_dir, event_type):
    test_string = "_test" if args.test else ""
    batch_string = f"_batch{args.batch_num}" if args.batch_num != 0 else ""
    dataset_string = f"{args.dataset}{args.year[-2:]}{args.campaign}" if args.dataset == 'data' else f"mc{args.year[-2:]}{args.campaign}_{args.dataset}"
    output_file = f"{output_dir}histo_{dataset_string}{event_type}{test_string}{batch_string}.root"
    return output_file

if __name__ == "__main__":
    # Reading arguments from the command line
    parser = argparse.ArgumentParser(description='Process some integers.')
    parser.add_argument('--test', type=int, help='run the code on a small number of events')
    parser.add_argument('--input_folder', type=str, help='input data or mc folder name')
    parser.add_argument('--output_folder', type=str, help='output folder name', default='/eos/user/l/lvannoli/higgs/histos/')
    parser.add_argument('--year', type=str, help='year eg: 2023', default='2023')
    parser.add_argument('--file_number', type=str, help='file number')
    parser.add_argument('--campaign', type=str, help='campaign letter', default='')
    parser.add_argument('--dataset', type=str, help='choose the dataset to run (z_jet, ttbar, vv, data, ttdb, llgamma)', choices=['z_jet', 'ttbar', 'vv', 'data', 'ttdb', 'llgamma'], required=True)
    parser.add_argument('--ev_batch', type=int, help='number of events to be processed per batch')
    parser.add_argument('--batch_num' , type=int, help='number of batch', default=0)
    parser.add_argument('--max_batches', type=int, help='max number of batches')
    parser.add_argument('--condor', action='store_true', help='disable tqdm printouts', default=False)
    args = parser.parse_args()

    print(f'Input folder: {args.input_folder}')
    print(f'Output folder: {args.output_folder}')
    
    # Ouput and input folder    
    output_dir = args.output_folder
    in_folder = args.input_folder
    
    # List of files to process
    files_to_process = os.listdir(in_folder)
    # Create a Manager object
    manager = Manager()
    # Create a Queue object
    print_queue = manager.Queue()
    # Number of process to start
    num_processes = multiprocessing.cpu_count()
    
    # Start processes in parallel
    with multiprocessing.Pool(num_processes) as pool:
        # Each process have an unique id
        #results = pool.starmap(process_file, [(file_name, idx) for idx, file_name in enumerate(files_to_process)]) 
        results = pool.starmap(process_file, [(file_name, idx, print_queue) for idx, file_name in enumerate(files_to_process)])       
    # Wait for all processes to finish
    pool.join()

    # Print the output from each process
    while not print_queue.empty():
        print(print_queue.get())
    # Merge the results of the processes
    data_list = [data for result in results for data in result if data]
    df = pd.DataFrame(data_list)

    print('DataFrame created')

    df_selected = {}
    print('---------------------------------------------------------------------')
    # Explode only the columns that have more than one element per row
    columns_to_explode = [col for col in df.columns if df[col].apply(lambda x: isinstance(x, list) and len(x) > 1).any()]
    df_expanded = df.explode(columns_to_explode).reset_index().rename(columns={'index': 'level_0'})
    del df
    event_counts = {event_type: 0 for event_type in configs['event_types'].keys()}
    for event_type, type_number in tqdm(configs['event_types'].items(), desc='splitting dataframes', disable=args.condor):
        event_counts[event_type] = len(df_expanded[df_expanded['event_type'] == type_number])
        data = df_expanded[df_expanded['event_type'] == type_number]
        leading = data.loc[data.groupby('level_0')['lepton_pt'].idxmax()]
        subleading = data.loc[data.groupby('level_0')['lepton_pt'].idxmin()]
        df_selected[f'leading{event_type}_{args.dataset}'] = leading
        df_selected[f'subleading{event_type}_{args.dataset}'] = subleading
    
    # Calculate the total number of events
    total_events = sum(event_counts.values())
    # Calculate the percentage of events for each event type relative to the total
    event_percentages = {event_type: (count / total_events) * 100 for event_type, count in event_counts.items()}

    # Print the percentages
    for event_type, percentage in event_percentages.items():
        if percentage != 0.0:
            print(f'Percentage of {event_type} events: {percentage}%')

    del df_expanded

    print('---------------------------------------------------------------------')
    histograms = {}
    for df_key, dataframe in tqdm(df_selected.items(), desc='filling histograms', disable=args.condor):
        event_type, process = df_key.split('_',1)
        process = process.split("_")[0]
        if dataframe.empty:
            continue
        histogram_dict = create_histograms(f'{process}_{event_type}')
        histograms.update(histogram_dict)
        for column in configs['histogram_props'].keys():
            fill_histograms(histogram_dict[f'{process}_{event_type}_{column}'], dataframe, column)
    print('Histograms filled')
    #print(f'histograms: {histograms.keys()}')
    del df_selected

    output_file = []
    for event_type in configs['event_types'].keys():
        event_histograms = {key: value for key, value in histograms.items() if f'{event_type[1:]}_' in key}
        if event_histograms:
            output_file = generate_output_file_path(args, output_dir, event_type)
            f = r.TFile(output_file, "RECREATE")
            f.cd()
            # Save histograms with event_type in the key of the histograms dictionary in the ROOT file
            for histogram in event_histograms.values():
                histogram.SetName(histogram.GetName().replace(f'{event_type[1:]}_', ''))
                histogram.Write()
            f.Close()
            print(f'ROOT file saved: {output_file}')