# Plottify

In this repo are contained all the scripts needed to generate the data/MC plots for the 2 leptons background of the Higgs in 4 leptons analysis.

## Histogram maker

To run the histograms maker you can run 

```bash
./run_condor.sh data_22 ttbar_a vv_a z_jet_a data_23 ttbar_c vv_c z_jet_c ttbar_d vv_d z_jet_d 2>&1 &
```

where the `data_22`, `ttbar_a` and so on are the samples.

The script firstly runs `make_histo.py` script. It runs in condor 10 jobs for each samples and create 10 root file (eg: histo_data22_2e_batch1.root).

After that, with a python code named `sum_histo.py` the histograms are added in a single histogram variable by varible.

The code createss different root files for the different samples and different leptons (eg: `histo_data22_2e.root` and `histo_data22_2mu.root`). 

If you already have the root file divided per batch, you can run the script `run_condor.sh` with the flag `--sum-only`. In this way it will run only `sum_histo.py` script.

### Running make_histo.py in test mode

It is possible to run the `make_histo.py` script in test mode. In this way the script will run only on few events. To do that you can run the following command:

```bash
python3 make_histo.py --test 1000 --dataset DATASET --input_folder INPUT_FOLDER --year YEAR --output_folder OUTPUT_FOLDER --campaign CAMPAIGN 
```

where:

- `DATASET` is the name of the dataset (eg: `data_22`, `ttbar_a`, `vv_a`, `z_jet_a`, `data_23`, `ttbar_c`, `vv_c`, `z_jet_c`, `ttbar_d`, `vv_d`, `z_jet_d`).
- `INPUT_FOLDER` is the path to the folder containing the input root files (eg: `/eos/atlas/atlascerngroupdisk/phys-higgs/HSG2/H4l/Run3/Minitrees/Prod_r3_v07/mc23${CAMPAIGN}/Background/BkgCRZ/`).
- `YEAR` is the year of the dataset (eg: `2022`, `2023`).
- `OUTPUT_FOLDER` is the path to the folder where the output root files will be saved (eg: `histos/DATASET/output/`).
- `CAMPAIGN` is the letter of the campaign (eg: `a`, `c`, `d`).
- `--test` is the flag to run the script in test mode and the numeber is the number of events you will run on.


## Plots maker

to run the plots maker you can copy the following line:

```bash
root -l -b -q 'plottify.C("channel", "which_lepton", "year", "campaign")' 
```

where:

- The `channel` could be `2e` or `2mu`.
- `which_letpon` could be `leading` or `subleading`. 
- The `year` could be `2022`, `2023` and so on.
- The `camapign` could be `a`, `c` or `d`.

while,

- `-b` instructs ROOT to run in batch mode, which disables graphics functionality.
- `-q` instructs ROOT to exit after executing the script.

With this script (Kindly offered by Guglielmo Frattari), a plot for each variable is generated. 

Each plot is composed of a stacked histogram with the MonteCarlo samples and the data are superimposed on the histogram. A ratio plots of the data/MC are drawn below.

