#!/bin/bash 
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
export ALRB_localConfigDir=$HOME/localConfig
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
lsetup "root 6.30.02-x86_64-el9-gcc13-opt"

BATCH_NUM=$(($1 + 1))
DATASET=$2 #data_23 or ttbar_a etc
CAMPAIGN=$3
PATH_TO_PLOTTIFY=$4

cp ${PATH_TO_PLOTTIFY}make_histo.py .
cp ${PATH_TO_PLOTTIFY}configs.json .

#aggiungi una condizione su input_folder
if [ $DATASET = "data" ]
then
    python3 make_histo.py --dataset data --input_folder /eos/home-l/lvannoli/higgs/data${CAMPAIGN}_v1/miniTrees/data/Background/BkgCRZ/ --year 20${CAMPAIGN} --output_folder ${PATH_TO_PLOTTIFY}histos/data/output/ --batch_num $BATCH_NUM --max_batches 10 --condor
else
    python3 make_histo.py --dataset $DATASET --input_folder /eos/atlas/atlascerngroupdisk/phys-higgs/HSG2/H4l/Run3/Minitrees/Prod_r3_v07/mc23${CAMPAIGN}/Background/BkgCRZ/ --year 2023 --output_folder ${PATH_TO_PLOTTIFY}histos/$DATASET/output/ --campaign $CAMPAIGN --batch_num $BATCH_NUM --max_batches 10 --condor
fi 
