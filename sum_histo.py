import ROOT as r
import os
import argparse
import json

with open('configs.json') as f:
    configs = json.load(f)

# Get by argument what kind of file to process
parser = argparse.ArgumentParser()
parser.add_argument('--dataset', type=str, help='choose the dataset to run (z_jet, ttbar, vv, data, ttdb, llgamma)', choices=['z_jet', 'ttbar', 'vv', 'data', 'ttdb', 'llgamma'], required=True)
parser.add_argument('--campaign', type=str, help='campaign letter', default='')
parser.add_argument('--year', type=str, help='year')
args = parser.parse_args()

# Path to the directory with the ROOT files
root_dir = f"histos/{args.dataset}/output/"
# Define output directory based on year and campaign
if args.year == '22' or args.campaign == 'a':
    out_dir = f"/eos/home-l/lvannoli/higgs/histos/condor/2022/"
elif args.year == '23' or (args.campaign == 'c' or args.campaign == 'd'):
    out_dir = f"/eos/home-l/lvannoli/higgs/histos/condor/2023/"
else:
    out_dir = f"/eos/home-l/lvannoli/higgs/histos/condor/{args.year}_{args.campaign}/"
#create the outdir if it does not exist
if not os.path.exists(out_dir):
    os.makedirs(out_dir)
file_name = ""

files = []
dataset_name = f"histo_data{args.year[-2:]}" if args.dataset == "data" else f"histo_mc{args.year[-2:]}{args.campaign}_{args.dataset}" 
event_files = {event_type: [] for event_type in configs['event_types'].keys()}

# Iterate over all ROOT files in the directory
for root_file in os.listdir(root_dir):
    if not root_file.endswith(".root"):
        continue
    elif dataset_name in root_file and 'batch' in root_file:
        #print('running on file:', root_file)
        # Check if the file name contains one of the keys of event_types
        for event_type in configs['event_types'].keys():
            if event_type in root_file:
                event_files[event_type].append(root_file)
#print(f'event_files:\n{event_files}')
for event_type, files in event_files.items():
    file = []
    if len(files) == 0:
        continue
    hist_sums = {}
    for i, root_file in enumerate(files):
        print('running on file:', root_file)
        file.append(r.TFile(os.path.join(root_dir, root_file)))
        # Iterate over all keys in the ROOT file
        for key in file[i].GetListOfKeys():
            hist = key.ReadObj()
            if hist is None:
                print(f'No histogram named {key.GetName()} in file {root_file}')
                continue
            # If the histogram is not already in the dictionary, add it
            if key.GetName() not in hist_sums:
                clone = hist.Clone()
                if clone is None:
                    print(f'Failed to clone histogram {key.GetName()}')
                    continue
                hist.SetDirectory(0)
                hist_sums[key.GetName()] = clone
                hist_sums[key.GetName()].SetName(key.GetName())
            else:
                # Otherwise, add the histogram to the existing sum
                hist_sums[key.GetName()].Add(hist)
    # Create a new ROOT file to save the histogram sums
    output_file = r.TFile(os.path.join(out_dir, f"{dataset_name}{event_type}.root"), "RECREATE")
    print('saving to file:', os.path.join(out_dir, f"{dataset_name}{event_type}.root"))
    output_file.cd()
    for hist in hist_sums.values():
        hist.Write()
    output_file.Write()
    output_file.Close()
    for f in file:
        f.Close()